package kot.bau.com.bau.github

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val URL = "https://api.github.com"

object RestManager {

    private val okHttp = OkHttpClient.Builder()
            .connectTimeout(200, TimeUnit.SECONDS)
            .build()

    private val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttp)
            .build()

    fun <T> getService(tClass: Class<T>): T = retrofit.create(tClass)

}