package kot.bau.com.bau.github

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import kot.bau.com.bau.BaseActivity
import kot.bau.com.bau.R
import kot.bau.com.bau.leaders.LeaderAdapter
import kotlinx.android.synthetic.main.github_activity.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GithubActivity : BaseActivity() {

    override fun getLayoutId() = R.layout.github_activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getGithubRepos()
    }

    private fun getGithubRepos() {
        val service = RestManager.getService(GithubService::class.java)
        service.getRepos().enqueue(object : Callback<GithubResponse> {
            override fun onFailure(call: Call<GithubResponse>, t: Throwable) {
                Log.e("hata", "hata")
            }

            override fun onResponse(call: Call<GithubResponse>, response: Response<GithubResponse>) {
                response.body()?.let {
                    fillViews(it)
                }
            }

        })
    }

    private fun fillViews(githubResponse: GithubResponse) {
        val llmanager = LinearLayoutManager(this)
        with(githubActivityRV) {
            layoutManager = llmanager
            addItemDecoration(DividerItemDecoration(this@GithubActivity, llmanager.orientation))
            adapter = LeaderAdapter(githubResponse.items, this@GithubActivity)
        }
    }
}