package kot.bau.com.bau.leaders

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kot.bau.com.bau.R
import kot.bau.com.bau.github.Item
import kotlinx.android.synthetic.main.github_row.view.*
import kotlinx.android.synthetic.main.leaders_row.view.*

class LeaderAdapter(private val items: List<Item>,
                    private val context: Context) : RecyclerView.Adapter<LeaderAdapter.LeaderViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeaderViewHolder {
        return LeaderViewHolder(LayoutInflater.from(context).inflate(R.layout.github_row, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(leadersHolder: LeaderViewHolder, position: Int) {
        with(items[position]) {
            with(leadersHolder) {
                Picasso.get().load(owner.avatar_url).into(githubPhotoIV)
                githubNameTV.text = full_name
                githubDescTV.text = description
                githubForkTV.text = forks_count.toString()
                githubStarTV.text = score.toString()
                githubWatchersTV.text = watchers_count.toString()
            }
        }
    }

    class LeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val githubPhotoIV = view.githubPhotoIV
        val githubNameTV = view.githubNameTV
        val githubDescTV = view.githubDescTV
        val githubForkTV = view.githubForkTV
        val githubStarTV = view.githubStarTV
        val githubWatchersTV = view.githubWatchersTV
    }
}