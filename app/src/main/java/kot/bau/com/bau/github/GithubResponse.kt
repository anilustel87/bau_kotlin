package kot.bau.com.bau.github

data class GithubResponse(val total_count: Int, val incomplete_results: Boolean, val items: List<Item>)

data class Item(val id: Int,
                val full_name: String,
                val owner: Owner,
                val private: Boolean,
                val html_url: String,
                val description: String,
                val watchers_count: Int,
                val forks_count: Int,
                val score: Double)

data class Owner(val id: Int, val avatar_url: String, val html_url: String, val site_admin: Boolean)