package kot.bau.com.bau.github

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubService {

    @GET("search/repositories")
    fun getRepos(@Query("q") search: String = "trending") : Call<GithubResponse>
}