package kot.bau.com.bau.leaders;

import java.util.Objects;

public class JLeadersData {

    private String name;
    private Long point;

    public JLeadersData(String name, Long point) {
        this.name = name;
        this.point = point;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPoint() {
        return point;
    }

    public void setPoint(Long point) {
        this.point = point;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JLeadersData that = (JLeadersData) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(point, that.point);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, point);
    }

    @Override
    public String toString() {
        return "JLeadersData{" +
                "name='" + name + '\'' +
                ", point=" + point +
                '}';
    }
}