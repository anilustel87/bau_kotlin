package kot.bau.com.bau.leaders

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kot.bau.com.bau.BaseActivity
import kot.bau.com.bau.R.layout.leaders_activity
import kotlinx.android.synthetic.main.leaders_activity.*

class LeadersActivity : BaseActivity() {

    override fun getLayoutId() = leaders_activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val mLayoutManager = LinearLayoutManager(this)
        with(leadersActivityRV) {
            layoutManager = mLayoutManager
//            adapter = LeaderAdapter(prepareLeadersData(), this@LeadersActivity)
        }
    }

    private fun prepareLeadersData(): MutableList<LeadersData> {
        val users = mutableListOf(
                LeadersData("AND", 1_000),
                LeadersData("dklsdsa"),
                LeadersData("dsadsk", 23),
                LeadersData("dskadksad", 50),
                LeadersData("dsaldklsaş", 150),
                LeadersData("flşidsflgND", 999),
                LeadersData("AfdlisşfdND", 899),
                LeadersData("dflişdsfAND", 0),
                LeadersData("fdişflidsşfAND", 59),
                LeadersData("fdilfisdAND", 34)
        )

        users[0].run {
            name = "şksdşd"
            point = 10000000
        }

        val filteredLeaders = users
                .filter { it.point > 0 }
                .sortedByDescending { it.point }

        return filteredLeaders.toMutableList()
    }
}