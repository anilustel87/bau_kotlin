package kot.bau.com.bau

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kot.bau.com.bau.R.layout.activity_main
import kot.bau.com.bau.github.GithubActivity
import kot.bau.com.bau.leaders.LeadersActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun getLayoutId() = activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainActivityTitleTV.text = "Hello Kotlin!"
        mainActivityBTN.setOnClickListener {
            startActivity(Intent(this, GithubActivity::class.java))
        }
    }
}
